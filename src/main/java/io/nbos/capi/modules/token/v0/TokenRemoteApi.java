package io.nbos.capi.modules.token.v0;

import io.nbos.capi.api.v0.models.TokenApiModel;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by vivekkiran on 8/11/16.
 */

public interface TokenRemoteApi {
    String tokenUrl = "/oauth/token";

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST(tokenUrl)
    Observable<TokenApiModel> getToken(@Field("client_id") String clientId, @Field("client_secret") String clientSecret, @Field("grant_type") String grantType);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST(tokenUrl)
    Observable<TokenApiModel> refreshAccessToken(@Field("client_id") String clientId,@Field("client_secret") String clientSecret, @Field("grant_type") String grant_type, @Field("refresh_token") String refresh_token);

}
