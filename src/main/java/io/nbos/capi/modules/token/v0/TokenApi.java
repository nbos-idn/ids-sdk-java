package io.nbos.capi.modules.token.v0;

import io.nbos.capi.api.v0.support.IdnCallback;
import io.nbos.capi.api.v0.support.NetworkApi;
import io.nbos.capi.api.v0.models.TokenApiModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.util.Map;

/**
 * Created by vivekkiran on 8/11/16.
 */

public class TokenApi extends NetworkApi {
    public TokenApi() {
        super();
        setModuleName("token");
        setRemoteApiClass(TokenRemoteApi.class);
    }

    public void getClientToken() {
        final TokenRemoteApi remoteApi = getRemoteApi();

        Map<String, String> map = apiContext.getClientCredentials();
        String clientId = map.get("client_id");
        String secret = map.get("client_secret");

        // The following works.
        Observable<TokenApiModel> call = remoteApi.getToken(clientId, secret, "client_credentials");
        call.subscribeOn(Schedulers.io()).observeOn(Schedulers.newThread()).subscribe(new Observer<TokenApiModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(TokenApiModel tokenApiModel) {
                System.out.println("token:" + tokenApiModel.getAccess_token());
                apiContext.setClientToken(tokenApiModel);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        });

    }

    public void refreshToken(final IdnCallback<TokenApiModel> callback) {
        final TokenRemoteApi remoteApi = getRemoteApi();
        Map map = apiContext.getClientCredentials();
        String clientId = (String) map.get("client_id");
        String clientSecret = (String) map.get("client_secret");
        String refreshToken = apiContext.getUserToken(moduleName).getRefresh_token();
        TokenApiModel tokenApiModel = null;
        // The following works.
        Observable<TokenApiModel> call = remoteApi.refreshAccessToken(clientId, clientSecret, "refresh_token", refreshToken);
        call.subscribeOn(Schedulers.io()).observeOn(Schedulers.newThread()).subscribe(new Observer<TokenApiModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(TokenApiModel tokenApiModel) {
                apiContext.setUserToken(moduleName, tokenApiModel);

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        });
    }
}
