package io.nbos.capi.api.v0.models;


public class RestMessage {

    public String getMessageCode() {
        return messageCode;
    }

    public String getMessage() {
        return message;
    }

    public String messageCode;
    public String message;

}
