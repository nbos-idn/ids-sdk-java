package io.nbos.capi.api.v0.support;


import io.nbos.capi.api.v0.context.ApiContext;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vivekkiran on 6/14/16.
 * <p>
 * -
 * com.nbos.io.nbos.capi.modules.ids.v0
 * IdsApi extends NetworkApi
 * IdsRemoteApi
 * -- necessary Ids models --
 */
/**
 *    -
 *    com.nbos.io.nbos.capi.modules.ids.v0
 IdsApi extends NetworkApi
 IdsRemoteApi
 -- necessary Ids models --
 */

/**
 * API associated with a module
 */
public class NetworkApi {

    protected String moduleName;
    protected String host;
    protected ApiContext apiContext;

    private Class<?> remoteApiClass;
    private Object remoteApi;

    public NetworkApi() {

    }

    public NetworkApi(String host, Class<?> remoteApiClass) {
        this.host = host;
        this.remoteApiClass = remoteApiClass;
    }

    public <T> T getRemoteApi() {
        if (remoteApi != null) {
            return (T) remoteApi;
        }
        try {
            remoteApi = getRetrofitClient().create(remoteApiClass);
            return (T) remoteApi;
        } catch (Exception x) {
            //  Log.i("IDS","unable to instantiate new object");
        }
        return null;
    }

    public void setApiContext(ApiContext apiContext) {
        this.apiContext = apiContext;
    }

    public void setHost(String host) {
        this.host = host;
        if (host.endsWith("/")) {
            this.host = host.substring(0, host.length() - 1);
        }
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getHost() {
        return host;
    }

    public Class<?> getRemoteApiClass() {
        return remoteApiClass;
    }

    public void setRemoteApiClass(Class<?> remoteApiClass) {
        this.remoteApiClass = remoteApiClass;
    }

    public Request.Builder newRequest(String api) {
        String endpoint = api;
        if (!api.startsWith("http")) {
            endpoint = host + api;
        }
        return new Request.Builder().url(endpoint);
    }

    protected Retrofit getRetrofitClient() {
        // TODO: get the host based on swagger
        String host = this.host;
        if( host == null ) host = apiContext.getHost(moduleName);
        return new Retrofit.Builder()
                .baseUrl(host)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    protected OkHttpClient getOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
}
