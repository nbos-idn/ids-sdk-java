package io.nbos.capi.api.v0.context;

import java.util.HashMap;

/**
 * Created by vivekkiran on 6/29/16.
 */

public abstract class AbstractApiContext implements ApiContext {
    protected String name = "app";
    private static HashMap<String, ApiContext> apiContexts = new HashMap<>();

    public AbstractApiContext() {
    }

    public AbstractApiContext(String name) {
        this.name = name;
    }


    // name of the context instance


    public static void registerApiContext(ApiContext apiContext) {
        apiContexts.put(apiContext.getName(), apiContext);
    }


    public static ApiContext get(String name) {
        ApiContext ctx = apiContexts.get(name);
        if (ctx == null) {
            ctx = new InMemoryApiContext(name);
            registerApiContext(ctx);
        }
        return ctx;
    }


}